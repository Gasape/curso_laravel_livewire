<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class PutRequest extends FormRequest
{

    static public function myRules() {
        return [
            'title' => 'required|min:2|max:255',
            'text' => 'required|min:10|max:5000',
            'description' => 'required|min:2|max:255',
            'posted' => 'required',
            'type' => 'required',
            'category_id' => 'required',
            'image' => 'nullable|image|max:1024',
        ];
    }
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    function failedValidation(Validator $validator)
    {
        if($this->expectsJson()){
            $response = new Response($validator->errors(),422);
            throw new ValidationException($validator, $response);
        }
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return $this->myRules();
    }
}
