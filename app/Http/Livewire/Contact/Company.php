<?php

namespace App\Http\Livewire\Contact;

use App\Models\ContactCompany;
use App\Models\ContactGeneral;
use Livewire\Component;

class Company extends Component
{
    public $personId;

    public function mount($personId){
        $this->personId = $personId;

        $c = ContactCompany::where('contact_general_id', $this->personId)->first();
        if($c != null){
            $this->name = $c->name;
            $this->identification = $c->identification;
            $this->extra = $c->extra;
            $this->email = $c->email;
            $this->choices = $c->choices;
        }
    }
    public $name;
    public $identification;
    public $email;
    public $extra;
    public $choices;

    protected $rules = [
        'name' => 'required|max:255',
        'identification' => 'required|max:50',
        'email' => 'required|max:80|min:2',
        'extra' => 'required',
        'choices' => 'required',
    ];

    public function render()
    {
        return view('livewire.contact.company');
        
    }

    public function stepBack(){
        $this->dispatch('stepEvent',1);
    }

    public function submit(){
        $this->validate();
    
        ContactCompany::updateOrCreate(
            ['contact_general_id' => $this->personId],
            [
                'name' => $this->name,
                'identification' => $this->identification,
                'extra' => $this->extra,
                'email' => $this->email,
                'choices' => $this->choices,
                'contact_general_id' => $this->personId,
            ]
    
        );
        $this->dispatch("stepEvent",3);

    }
}
