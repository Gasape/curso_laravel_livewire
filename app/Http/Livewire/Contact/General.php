<?php

namespace App\Http\Livewire\Contact;

use App\Models\ContactCompany;
use App\Models\ContactGeneral;
use App\Models\ContactPerson;
use Livewire\Component;

class General extends Component
{
    public $personId;
    public $subject;
    public $type;
    public $message;
    public $step =  1;

    protected $listeners=[
        'stepEvent' => 'stepEvent',
    ];

    protected $rules = [
        'subject' => 'required|min:2|max:255',
        'type' => 'required|min:2',
        'message' => 'required',
    ];

    public function mount($subject = ""){
        $this->subject = $subject;
    }

    public function render()
    {
        return view('livewire.contact.general')->layout('layouts.contact');
    }

    public function submit(){

        $this->validate();
        $this->personId = ContactGeneral::create([
                'subject' => $this->subject,
                'type' => $this->type,
                'message' => $this->message,
            ])->id;
        $this->dispatch('stepEvent',2);
    }


    public function stepEvent($step){
       if($step == 2){  
           if($this->type == "company"){
               $this->step = 2;
            }
            elseif($this->type == "person"){
                $this->step = 2.5;
            }
        }else{
            $this->step = $step; 
        }

    }
}
