<?php

namespace App\Http\Livewire\Contact;

use App\Models\ContactGeneral;
use App\Models\ContactPerson;
use Livewire\Component;

class Person extends Component
{
    public $personId;

    public function mount($personId){
        $this->personId = $personId;

        $c = ContactPerson::where('contact_general_id', $this->personId)->first();
        if($c != null){
            $this->name = $c->name;
            $this->surname = $c->surname;
            $this->choices = $c->choices;
            $this->others = $c->others;

        }
    }
    
    protected $rules = [
        'name' => 'required|max:255',
        'surname' => 'required|max:80',
        'choices' => 'required',
        'others' => 'nullable',
    ];

    public $name;
    public $surname;
    public $choices;
    public $others;

    public function render()
    {
        return view('livewire.contact.person');
    }

    public function stepBack(){
        $this->dispatch('stepEvent',1);
    }


    public function submit(){
        $this->validate();
        
        ContactPerson::updateOrCreate(
            ['contact_general_id' => $this->personId],
            [
            'name' => $this->name,
            'surname' => $this->surname,
            'choices' => $this->choices,
            'others' => $this->others,
            'contact_general_id' => $this->personId,
        ]);
        $this->dispatch('stepEvent',3);
    }
}
