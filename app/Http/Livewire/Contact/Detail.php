<?php

namespace App\Http\Livewire\Contact;

use App\Models\ContactCompany;
use App\Models\ContactDetail;
use App\Models\ContactGeneral;
use Livewire\Component;

class Detail extends Component
{
    
    public $extra;
    public $personId;

    public function mount($personId){
        $this->personId = $personId;

        $c = ContactDetail::where('contact_general_id', $this->personId)->first();
        if($c != null){
            $this->extra = $c->extra; 
        }
    }

    protected $rules = [ 
        'extra' => 'required|min:2|max:500',
    ];

    public function render()
    {
        return view('livewire.contact.detail');
    }

    public function stepBack(){
        $this->dispatch('stepEvent',2);
    }

    public function submit(){
        
        $this->validate();
        
        ContactDetail::updateOrCreate(
            ['contact_general_id' => $this->personId],
            [
            'extra' => $this->extra, 
            'contact_general_id' => $this->personId,
        ]);

        $this->dispatch('stepEvent',4);
    }
}
