<?php

namespace App\Http\Livewire\Blog;

use App\Models\Category;
use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $queryString = ['type','posted','category_id','search','from','to','sortColumn','sortDirection'];

    public $conmfirmingDeletePost;
    public $postToDelete;

    //filtros
    public $posted;
    public $type;
    public $category_id;
    public $contador = 1;

    //busquedas
    public $search; 

    //dates
    public $from;
    public $to;

    //order
    public $columns=[
        'id' => "Id",
        'title' => "Título",
        'date' => "Fecha",
        'description' => "Descripción",
        'posted' => "posted",
        'type' => "tipo",
        'category_id' => "Cateoría",
   ];

    public function render()
    {
        $query = Post::where('posted','yes');
        $categories = Category::pluck("title","id");
        $query = $this->filtro($query);
        $posts = $query->paginate(10);
        return view('livewire.Blog.index',compact('posts','categories'))->layout('layouts.web');
    }
    
    function filtro($query){
        if($this->type){
            $query->where('type', $this->type);
            
        }
        if($this->category_id ){
            $query->where('category_id', $this->category_id);
            
        }
        if($this->posted){
            $query->where('posted', $this->posted);
            
        }
        if($this->search){
            $query = $query->where(function($subquery){
                $subquery->orwhere('id','like','%'.$this->search.'%');
                $subquery->orwhere('description','like','%'.$this->search.'%');
                $subquery->orWhere('title','like','%'.$this->search.'%');
            });
            
        }
        if($this->from && $this->to){
            $query->whereBetween('date', [date($this->from),date($this->to)]);
            
        }

        return $query;
    }




}
?>

