<?php

namespace App\Http\Livewire\Dashboard\Post;

use App\Models\Category;
use Livewire\Component;
use App\Models\Post;
use Illuminate\Support\Facades\Log;
use Livewire\WithFileUploads;

class Save extends Component
{

    use WithFileUploads;

    public $title;
    public $date;
    public $text;
    public $description;
    public $posted;
    public $type;
    public $category_id;
    public $image;
    
    public $post;
    protected $rules =[
        'title' => 'required|min:2|max:255',
        'date' => 'required',
        'text' => 'required|min:2|max:5000',
        'description' => 'required|min:2|max:255',
        'posted' => 'required',
        'type' => 'required',
        'category_id' => 'required',
        'image' => 'nullable|image|max:1024',
    ];

    public function mount($id = null){

        if($id != null){

            $this->post = Post::findOrFail($id);
            $this->title = $this->post->title;
            $this->text = $this->post->text;
            $this->date = $this->post->date;
            $this->description = $this->post->description;
            $this->posted = $this->post->posted;
            $this->type = $this->post->type;
            $this->category_id = $this->post->category_id;
        }
        
    }


    public function render()
    {
        $categories = Category::get();
        return view('livewire.dashboard.post.save', compact('categories'));
    }

    public function submit(){

        //validación del formulario, que no se puedan ingresar campos vacios
        $this->validate();

        //save
        //ingreso del formulario 
        if($this->post)
        {
            $this->post->update([

                'title' => $this->title,
                'text' => $this->text,
                'description' => $this->description,
                'date' => $this->date,
                'posted' => $this->posted,
                'type' => $this->type,
                'category_id' => $this->category_id,
            ]);
            $this->dispatch('update');
            
        }else {
            $this->post = Post::create(
            [
               'title' => $this->title,
                'slug' => str($this->title)->slug(),
                'text' => $this->text,
                'description' => $this->description,
                'date' => $this->date,
                'posted' => $this->posted,
                'type' => $this->type,
                'category_id' => $this->category_id,
                ]
            );
            $this->dispatch('created');
        }

        //upload
        if($this->image){
            $imageName = $this->post->slug . '.' . $this->image->getClientOriginalExtension();

            $this->image->storePubliclyAs('images/post',$imageName,'public_upload');

            $this->post->update([
                'image'=> $imageName
            ]);
        } 

        //reinicio de los campos después de realizar el formulario para un toque mas estetico
        //$this->reset(['title','text']);

    }

    // public function boot(){
    //     Log::info("boot");
    // }
    // public function booted(){
    //     Log::info("booted");
    // }

    // public function mount(){
    //     Log::info("mount");
    // }

    // public function hydrateTitle($value){
    //     Log::info("hydrateTitle $value");
    // }

    // public function dehydrateFoo($value){
    //     Log::info("dehydrateFoo $value");
    // }

    // public function hydrate(){
    //     Log::info("hydrate");
    // }

    // public function dehydrate(){
    //     Log::info("dehydrate");
    // }

    // public function updating($name, $values){
    //     Log::info("updating $name $values");
    // }
    
    // public function updated($name, $values){
    //     Log::info("updated $name $values");
    // }
    
    // public function updatingTitle($values){
    //     Log::info("updatingTitle $values");
    // }
    // public function updatedTitle($values){
    //     Log::info("updatedTitle $values");
    // }
    
}
