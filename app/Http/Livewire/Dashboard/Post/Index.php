<?php

namespace App\Http\Livewire\Dashboard\Post;

use App\Http\Livewire\Dashboard\OrderTrait;
use App\Models\Category;
use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    protected $queryString = ['type','posted','category_id','search','from','to','sortColumn','sortDirection'];

    use WithPagination;
    use OrderTrait;

    public $conmfirmingDeletePost;
    public $postToDelete;

    //filtros
    public $posted;
    public $type;
    public $category_id;
    public $contador = 1;
    

    //busquedas
    public $search; 

    //dates
    public $from;
    public $to;

    //order
    public $columns=[
        'id' => "Id",
        'title' => "Título",
        'date' => "Fecha",
        'description' => "Descripción",
        'posted' => "posted",
        'type' => "tipo",
        'category_id' => "Cateoría",
   ];

    public function render()
    {
        
        
        $query = Post::orderBy($this->sortColumn,$this->sortDirection);
        $categories = Category::pluck("title","id");
        $query = $this->filtro($query);
        $posts = $query->paginate(10);
        return view('livewire.dashboard.post.index',compact('posts','categories'));
    }
    
    function filtro($query){
        if($this->type){
            $query->where('type', $this->type);
            
        }
        if($this->category_id ){
            $query->where('category_id', $this->category_id);
            
        }
        if($this->posted){
            $query->where('posted', $this->posted);
            
        }
        if($this->search){
            $query = $query->where(function($subquery){
                $subquery->orwhere('id','like','%'.$this->search.'%');
                $subquery->orwhere('description','like','%'.$this->search.'%');
                $subquery->orWhere('title','like','%'.$this->search.'%');
            });
            
        }
        if($this->from && $this->to){
            $query->whereBetween('date', [date($this->from),date($this->to)]);
            
        }

        return $query;
    }

    function selectedPostToDelete(Post $post){
        $this->conmfirmingDeletePost = true;
        $this->postToDelete = $post;
    }

    public function delete(){
        $this->postToDelete->delete();
        $this->conmfirmingDeletePost = false;
        $this->dispatch('Deleted');
        
    }

}
