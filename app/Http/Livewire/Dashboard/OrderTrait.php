<?php
namespace App\Http\Livewire\Dashboard;

trait OrderTrait{
    //order

    public $sortColumn = "id";
    public $sortDirection ="asc";

    function sort($column){
        $this->sortColumn = $column;
        $this->sortDirection = $this->sortDirection == 'asc' ? 'desc' : 'asc';
        if($this->contador == 1){
            $this->resetPage();
            $this->contador = $this->contador + 1;
        }else{
            $this->contador = $this->contador - 1;
        }
    }

}
?>