<?php

namespace App\Http\Livewire\Dashboard\Category;

use App\Http\Livewire\Dashboard\OrderTrait;
use App\Models\Category;
use Livewire\Attributes\Computed;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    use OrderTrait;
  
    public $conmfirmingDeleteCategory;
    public $categoryToDelete;

    #[Computed]
    public function getCategoryProperty() {
        if($this->categoryToDelete != null){
            return Category::find($this->categoryToDelete);
        }
        return "Sin categoría seleccionada";
    }

    public $contador = 1;

    public $columns=[
        'id' => 'Id',
        'title' => "Título",
   ];
   
    
    public function render()
    {
        $categories = Category::orderBy($this->sortColumn,$this->sortDirection)->paginate(10);
        return view('livewire.dashboard.category.index',compact('categories'));
    }

    function selectedCategoryToDelete(Category $category){
        $this->conmfirmingDeleteCategory = true;
        $this->categoryToDelete = $category;
    }

    public function delete(){
        $this->dispatch('Deleted');
        $this->conmfirmingDeleteCategory = false;
        $this->categoryToDelete->delete();
    }

}
