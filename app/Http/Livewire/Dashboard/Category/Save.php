<?php

namespace App\Http\Livewire\Dashboard\Category;

use Livewire\Component;
use App\Models\Category;
use Illuminate\Support\Facades\Log;
use Livewire\WithFileUploads;

class Save extends Component
{

    use WithFileUploads;

    public $title;
    public $text;
    public $category;
    public $image;

    protected $rules =[
        'title' => 'required|min:2|max:255',
        'text' => 'nullable',
        'image' => 'nullable|image|max:1024',
    ];

    public function mount($id = null){

        if($id != null){

            $this->category = Category::findOrFail($id);
            $this->title = $this->category->title;
            $this->text = $this->category->text;
        }

    }


    public function render()
    {
        return view('livewire.dashboard.category.save');
    }

    public function submit(){

        //validación del formulario, que no se puedan ingresar campos vacios
        $this->validate();

        //save
        //ingreso del formulario 
        if($this->category)
        {
            $this->category->update([

                'title' => $this->title,
                'text' => $this->text,
            ]);
            $this->dispatch('update');
            
        }else {
            $this->category = Category::create(
            [
                'title' => $this->title,
                'slug' => str($this->title)->slug(),
                'text' => $this->text,
                ]
            );
            $this->dispatch('created');
        }

        //upload
        if($this->image){
            $imageName = $this->category->slug . '.' . $this->image->getClientOriginalExtension();

            $this->image->storePubliclyAs('images/category',$imageName,'public_upload');

            $this->category->update([
                'image'=> $imageName
            ]);
        } 

        //reinicio de los campos después de realizar el formulario para un toque mas estetico
        //$this->reset(['title','text']);

    }

    // public function boot(){
    //     Log::info("boot");
    // }
    // public function booted(){
    //     Log::info("booted");
    // }

    // public function mount(){
    //     Log::info("mount");
    // }

    // public function hydrateTitle($value){
    //     Log::info("hydrateTitle $value");
    // }

    // public function dehydrateFoo($value){
    //     Log::info("dehydrateFoo $value");
    // }

    // public function hydrate(){
    //     Log::info("hydrate");
    // }

    // public function dehydrate(){
    //     Log::info("dehydrate");
    // }

    // public function updating($name, $values){
    //     Log::info("updating $name $values");
    // }
    
    // public function updated($name, $values){
    //     Log::info("updated $name $values");
    // }
    
    // public function updatingTitle($values){
    //     Log::info("updatingTitle $values");
    // }
    // public function updatedTitle($values){
    //     Log::info("updatedTitle $values");
    // }
    
}
