<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\PutRequest;
use App\Http\Requests\Category\StoreRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    
    public function index()
    {
        return response()->json(Category::paginate(10));
    }

    public function all()
    {
        return response()->json(Category::get());
    }

    public function slug($slug){
        $category = Category::where("slug", $slug)->firstOrFail();
        return response()->json($category);
    }
   
    public function store(StoreRequest $request)
    {
        if($request->validated()){
            return response()->json(Category::create([
                'title' => $request['title'],
                'slug' => str($request['title'])->slug(),
                'text' => $request['text'],
            ]));
        }else{
            return response()->json($request->validated());
        }
    }

   
    public function show(Category $category)
    {
        return response()->json($category);
        
    }

    
    public function update(PutRequest $request, Category $category)
    {
        
        $category->update($request->validated());
        return response()->json($category);

        // dd($request->validated());
        // if($request->validated() == false){
        //     return response()->json("ha ocurrido un error");
        // }else{
        //     $category->update($request->validated());   
        //     return response()->json("Se ha actualizado exitosamente");
        // }
    }

    
    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json("Eliminado exitosamente");
    }
    public function posts(Category $category){
        /*
        empezamos a declarar que traerá información que coincide en ambas tablas es decir un inner join el primer parametro es el nombre especifico de 
        la tabla que se une con la segunda tabla en este caso categories de la BD y lo siguiente es la condición de la union es decir donde el id de la
        tabla categories sea igual a la columna "category_id" de la tabla posts y el resto es simplemete SQL  
        */ 
        // Query Builder
        // $posts = Post::join('categories', "categories.id","=","posts.category_id")
        // // seleccionamos todo de la tabla Post de la base de datos y solo el titulo de la categoria 
        // ->select("posts.*", "categories.title as category")
        // // seleccionamos especificamente lo que queremos traer en este caso transformado a SQL sería where category_id = @category_id
        // ->where("category.id", $category->id)
        // Indicamos que traerá todo 
        // ->get();
        // ver la consulta en codigo sql
        // ->toSql();

        // colocamos el nombre exacto de la relación además el nombre con el que lo identificamos el modelo
        // Eloquent
        $posts = Post::with("category")->where("category_id", $category->id)->get();

        return response()->json($posts);
    }
}
