<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\PutRequest;
use App\Http\Requests\Post\StoreRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    // Regresa los post de manera paginada 
    public function index()
    {
        return response()->json(Post::paginate(10));
    }

    public function all(){
        return response()->json(Post::get());
    }

    public function slug(Post $post){ // $slug
        // $post = Post::with("category")->where("slug", $slug)->firstOrFail();
        // $post->category;
        return response()->json($post);
    }
    public function store(StoreRequest $request)
    {
        if($request->validated()){
            return response()->json(Post::create([
                'title' => $request['title'],
                'slug' => str($request['title'])->slug(),
                'text' => $request['text'],
                'description' => $request['description'],
                'date' => $request['date'],
                'posted' => $request['posted'],
                'type' => $request['type'],
                'category_id' => $request['category_id'],
            ]));
        }else{
            return response()->json($request->validated());
        }
    }

   
    public function show(Post $post)
    {
        return response()->json($post);
        
    }

    
    public function update(PutRequest $request, Post $post)
    {
        
        $post->update($request->validated());
        return response()->json($post);

        // dd($request->validated());
        // if($request->validated() == false){
        //     return response()->json("ha ocurrido un error");
        // }else{
        //     $post->update($request->validated());   
        //     return response()->json("Se ha actualizado exitosamente");
        // }
    }

    
    public function destroy(Post $post)
    {
        $post->delete();
        return response()->json("Eliminado exitosamente");
    }
}
