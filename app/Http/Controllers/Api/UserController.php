<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

use function Laravel\Prompts\error;

class UserController extends Controller
{
    public function CrearToken(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|',
            'name' => 'required|',
        ]);
        $credential = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if(Auth::attempt($credential)){
            $token = $request->user()->createToken($request->name);
            return response()->json(['token' => $token->plainTextToken]);
        }
        throw ValidationException::withMessages([
            'error' => ['Las credenciales son incorrectas'],
        ]);

    }
    public function EliminarToken(Request $request){
        if(Auth::check()){
            $affectedRows = $request->user()->currentAccessToken()->delete();
            return response()->json(['Deleted' => $affectedRows]);
        }
        return response()->json("Usted no ha iniciado sesión", 422);
    }
}
