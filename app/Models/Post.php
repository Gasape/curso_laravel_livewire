<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Post extends Model
{
    use HasFactory;

    protected $fillable=['title', 'slug', 'date', 'image', 'text', 'description', 'posted','type', 'category_id'];

    protected $cast = [
        'date' => 'datetime',
    ];

    public function getFormatedDate($fecha){
        return $fecha = Carbon::Parse($this->date)->format('d-m-Y');
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function tags(){
        return $this->morphToMany(Tag::class,'taggables');
    }

    public function getImageUrl(){
        if($this->image == null){
            return URL::asset("images/pokemon.png");
        }
        return URL::asset("images/Post/" .$this->image);

    }
}
