import './bootstrap';
import toast from 'toast-me';
import persist from '@alpinejs/persist';


// toast('Hola desde node')

window.toast = toast

// document.addEventListener('DOMContentLoaded', function() {
//     console.log('funciona')
//     window.Livewire.on('itemChange', () => {
//         toast('Se ha modificado el item')
//     })  
// })

const options = {
    position: 'bottom',
    timeoutOnRemove: 1000,
    duration: 1000,
}

Livewire.on('itemChange', (post) => {
    console.log(post[0])
    toast('Se ha actualizado el item ' + post[0].title,options);
})
Livewire.on('itemAdd', (post) => {
    console.log(post[0])
    toast('Se ha agregado el item ' + post[0].title,options);
})
Livewire.on('itemDelete', (post) => {
    console.log(post[0])
    toast('Se ha eliminado el item ' + post[0].title, 'error');
})

