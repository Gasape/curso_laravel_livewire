
{{-- parent id {{ $name }} --}}
<form wire:submit.prevent="submit" class="flex flex-col max-w-sm mx-auto">
    <x-label>{{ __('Name')}}</x-label>
    <x-input-error for="name"/>
    <x-input type="text" wire:model="name" />

    
    <x-label>{{ __('Surname')}}</x-label>
    <x-input-error for="surname"/>
    <x-input type="text" wire:model="surname" />
    

    <x-label>{{ __('Choices')}}</x-label>
    <x-input-error for="choices"/> 
    <select wire:model="choices">
        <option value="">Select</option>
        <option value="adverd"> {{ __('Adverd') }} </option>
        <option value="post"> {{ __('Post') }} </option>
        <option value="course"> {{ __('Course') }} </option>
        <option value="movie"> {{ __('Movie') }} </option>
        <option value="other"> {{ __('Other') }} </option>
    </select>


    <x-label>{{ __('Others')}}</x-label>
    <x-input-error for="others"/>
    <textarea wire:model="others"></textarea>


    <div class="flex mt-5 gap-5">
        <x-secondary-button wire:click="stepBack">Atrás</x-secondary-button>
        <x-button type="submit">Enviar</x-button>
    </div>
</form>

