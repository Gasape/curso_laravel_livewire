<div>
    @if ($item)
        <div class="box mb-3">
            <p>
                <input wire:keydown.enter="update" class="w-20" type="number" wire:model.live.debounce.250ms="count">
                {{ $item[0]['title'] }}
                <button wire:click="update">Guardar</button>
            </p>
        </div>    
    @endif
    
</div>
