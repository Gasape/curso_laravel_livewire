<div>
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"> --}}
    <div x-data="data()" x-init="ordenar()">
        <x-card class="container mt-5">  

            @slot('title')
                Total tareas <span x-text="totalTareas"></span>
            @endslot

            <label>Buscar</label>
            <x-input type="text" x-model="search"/>  

            <form wire:submit.prevent="save()" class="flex gap-2 mt-2">
                <label class="mt-2">Crear</label>
                <x-input type="text" wire:model="task"/>
                <x-button type="submit" class="btn btn-success">Agregar</x-button>
            </form>

            <ul x-ref="items" class="my-3">
                <template x-for="t in filterTodo()">
                    <li class="border py-3 px-4" :id="t.id" :count="t.count">
                        {{-- <input x-on:click=" $wire.status(t.id,!t.status); console.log(!t.status);" x-init="const ayuda = !t.status; t.status = ayuda" type="checkbox" x-model="t.status"> --}}
                        <input x-on:change="status(t)" type="checkbox" x-model="t.status" :checked="t.status == 1">
                        <span x-on:click="t.editMode=true" x-show="!t.editMode" x-text="t.name"></span>
                        <input x-on:keyup.enter="t.editMode=false; $wire.actualizar(t)" type="text" x-show="t.editMode" x-model="t.name">
                        <button class="float-end" x-on:click="eliminar(t)"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6"><path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                          </svg>
                          </button>
                    </li>
                </template>
            </ul>
            <div class="flex flex-row-reverse">
                <x-danger-button x-on:click="$wire.deleteTodos(); todos = []; ">Borrar las tareas</x-danger-button>
            </div>
        </x-card>
    </div>
</div>

<script>
    console.log(data.todos)
    function data(){
            return {
                search:"",
                task:"",
                // todos: Alpine.$persist(@entangle('todos')),
                todos: @entangle('todos'), //ordenación 1 y 2
                // todos: @json($todos),
                ordenar(){
                    Sortable.create(this.$refs.items, {
                        onEnd: (event) => {
                            
                            // Ordenación 1
                            // var todosAux = []
                            // document.querySelectorAll(".list-group li").forEach(todoHTML => {
                            //     this.todos.forEach(todo => {
                            //         if(todo.id == todoHTML.id){
                            //             todosAux.push(todo)
                            //         }
                            //     })
                            // })
                            // this.todos = todosAux
                            // Ordenación 1 


                            // Ordenación 2
                            // var t = this.todos[event.oldIndex]
                            // this.todos.splice(event.oldIndex, 1)
                            // this.todos.splice(event.newIndex, 0, t)
                            // Livewire.dispatch("refrescar");
                            // Livewire.dispatch("setOrden")
                            // Ordenación 2
                            
                            // Ordenación 3 por indices 
                            var ids = [];
                            document.querySelectorAll(".list-group li").forEach(todoHTML => {
                                ids.push(todoHTML.id)
                            })
                            
                            axios.post('/todo/re-orden',{
                                ids
                            });

                            // Livewire.dispatch('setOrdenById', { ids: ids })

                        }
                    })
                },
                // completed(todo){
                //     return todo.completed
                // },
                // incompleted(todo){
                //     return !todo.completed
                // },
                totalTareas(){
                    return this.todos.length
                },
                filterTodo(){
                    // return this.todos.filter(function(t){
                        //     console.log(this.search.toLowerCase())
                        //     return  t.task.toLowerCase().includes(this.search.toLowerCase())
                        // }.bind(this))
                        return this.todos.filter((t) => t.name.toLowerCase().includes(this.search.toLowerCase()))
                },
                eliminar(todo){
                    this.todos = this.todos.filter((t) => t != todo)
                    console.log(todo.id)
                    Livewire.dispatch('deleteById', { id: todo.id})
                },
                status(todo){
                    console.log(todo)
                    Livewire.dispatch('status', { id: todo.id, status: todo.status})
                },
                save(){
                    this.todos.push({
                        status:false,
                        task: this.task,
                    })
                    this.task = '' 

                },
                
            }
        }
</script>    

