<div>
    <x-card>
        <div class="grid grid-cols-2 mb-2 gap-2">
            <x-input class="w-full" type="text" wire:model.live.debounce.250ms="search" placeholder="Buscar por 'id', titulo o descripción"></x-input>
            <div class="grid grid-cols-2 gap-2">
                <x-input class="w-full" type="date" wire:model.change="from" placeholder="Desde"></x-input>
                <x-input class="w-full" type="date" wire:model.change="to" placeholder="Hasta"></x-input>
            </div>
        </div>
        <div class="flex gap-2 mb-2">
            <select class="block w-full" wire:model.change="type">
                <option value="">{{__("Type")}}</option>
                <option value="adverd">Adverd</option>
                <option value="course">Course</option>
                <option value="Movie">Movie</option>
            </select>

            <select class="block w-full" wire:model.change="category_id">
                <option value="">{{__("Category")}}</option>
                @foreach ($categories as $i => $c)
                    <option value="{{ $i }}">{{ $c }}</option>
                @endforeach
            </select>
        </div>
    </x-card>

    <div class="flex felx-col items-center mt-5">
        <div class="w-full overflow-hidden">    
            @foreach ($posts as $p)
                <center>
                    <h4 class="text-center text-4xl mb-3">{{ $p->title}}</h4>
                    <p class="text-center text-sm text-gray-500 italic font-bold uppercase tracking-widest">{{ $p->getFormatedDate($p->date) }}</p>
                    <img class="w-6/12 rounded-md my-3" src="{{ $p->getImageUrl()}}">
                    <p class="mx-4 text-justify">{{ $p->description}}</p>   
                    <div class="flex flex-col items-center mt-5">

                        <a class="btn-primary" href="{{route('web-show',$p->slug)}}">¡Leer el post!</a>
                    </div>

                    <hr class="my-16">
                </center>
            @endforeach
            </div>
    </div>

    {{ $posts->links()}}
</div>
