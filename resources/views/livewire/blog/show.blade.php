<div>
    <x-card>
        <center>
            <img class="w-6/12 rounded-md my-3" src="{{ $post->getImageUrl()}}" >
        </center>
        
        <h1 class="text-center text-5xl mb-3">{{ $post->title}}</h1>

        @livewire('shop.cart', ['post' => $post, 'type' => 'add'])
        
        <p class="my-4 ml-2">
            <span class="text-sm text-gray-500 italic font-bold uppercase tracking-widest px-5">{{ $post->getFormatedDate($post->date) }}</span>
            <span class="ml-4 rounded-md bg-purple-500 py-1 px-3 text-white">{{ $post->category->title}}</span>
            <span class="ml-4 rounded-md bg-purple-500 py-1 px-3 text-white">{{ $post->type}}</span>
        </p>
        
        <div class="mx-4 text-justify">{!! $post->text !!}</div>   
        
        <hr class="my-8">

        @livewire('contact.general', ['subject' => "#$post->id - "])
        
    </x-card>
</div>
