@slot('header')
    {{ __("CRUD categorías")}}
@endslot

    
<x-card class="container"> 

    <x-action-message on="Deleted">
        <div class="box-action-message">
            {{ __('Se ha borrado la categoría')  }}
        </div>
    </x-action-message>
    @slot('title')
        Listado
    @endslot
    <p>Categoria seleccionada {{ $this->category}}</p>

    <a class="btn-secondary mb-3" href="{{ route('d-category-create') }}">Crear</a>
    <table class="table w-full border text-center">
        <thead class=" bg-gray-100 text-center" >
            <tr class="border-b">
                @foreach ($columns as $key => $c)
                <th class="max-w-48 border-b p-2"> 
                    <button wire:click="sort('{{ $key }}')">
                        {{ $c }}
                    @if ($key == $sortColumn)
                        @if ($sortDirection == 'asc')
                            &uarr;
                        @else
                            &darr;
                        @endif
                        
                    </button>
                    @endif
                </th>
            @endforeach
                <th class="border-b p-2">
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $c)
                <tr class="border-b">
                    <td class="p-2 ">
                        {{ $c->id}}
                    </td>
                    <td class="p-2 ">
                        {{ $c->title}}
                    </td>
                    <td class="p-2">
                        <x-nav-link href="{{ route('d-category-edit', $c)}}" class="mr-3 btn-edit">Editar</x-nav-link>
                        <x-danger-button 
                        {{-- onclick="confirm('¿Seguro que desea eliminar el registro seleccionado?') || event.stopImmediatePropagation()"   --}}
                        wire:click="selectedCategoryToDelete({{$c}})">
                            Eliminar
                        </x-danger-button>
                        
                    </td>
                    <td>

                    </td>
                </tr>
            @endforeach
        </tbody>     
    </table>

    <br>

    {{ $categories->links() }}

    <x-confirmation-modal wire:model.live="conmfirmingDeleteCategory">
        <x-slot name="title">
            {{ __('Borrar Categoría') }}
        </x-slot>

        <x-slot name="content">
            {{ __('¿Estás seguro de querer eliminar'.$this->category.' esta categoria?') }}
        </x-slot>

        <x-slot name="footer">
            <x-secondary-button wire:click="$toggle('conmfirmingDeleteCategory')" wire:loading.attr="disabled">
                {{ __('Cancelar') }}
            </x-secondary-button>

            <x-danger-button class="ms-3" wire:click="delete" wire:loading.attr="disabled">
                {{ __('Eliminar') }}
            </x-danger-button>
        </x-slot>
    </x-confirmation-modal>

</x-card>
