@slot('header')
    {{ __("CRUD categorías")}}
@endslot
<div class="container">
    <x-action-message on="created">
        <div class="box-action-message">
            {{ __('Creado')  }}
        </div>
    </x-action-message>

    <x-action-message on="update">
        <div class="box-action-message">
            {{ __('Actualizado')  }}
        </div>
    </x-action-message>

    <x-form-section submit="submit">

        @slot('title')
            {{ __('Categories') }}
        @endslot

        @slot('description')
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Labore, blanditiis officiis. Ab suscipit facere quam ratione excepturi quo distinctio ullam laboriosam assumenda laudantium culpa dolor, ipsam dolore officiis sint perferendis?
        @endslot
       
        @slot('form')
            <div class="col-span-6 sm:col-span-4">
                <x-label for="">Titulo</x-label>
                <x-input-error for="title"/>
                <x-input class="block w-full" type="text" wire:model="title"/>
                
            </div>
    
            <div class="col-span-6 sm:col-span-4">
                <x-label for="">Texto</x-label>
                <x-input-error for="text"/>
                <x-input class="block w-full" type="text" wire:model="text"/>
            </div>

            <div class="col-span-6 sm:col-span-4">           
                <x-label for="">Imagen</x-label>
                <x-input-error for="image"/>
                <x-input type="file" wire:model="image"/>
                
                @if ($category && $category->image)
                <img class="w-40 my-3" src=" {{ $category->getImageUrl() }}">         
                @endif
            </div>
        @endslot
        
        @slot('actions')
            <x-button type="submit">Enviar</x-button>
        @endslot
    </x-form-section>

</div>
