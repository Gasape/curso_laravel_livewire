@slot('header')
    {{ __("CRUD categorías")}}
@endslot

    
<x-card class="container"> 

    <x-action-message on="Deleted">
        <div class="box-action-message">
            {{ __('Se ha borrado la categoría')  }}
        </div>
    </x-action-message>
    @slot('title')
        Listado
    @endslot

    <a class="btn-secondary mb-3" href="{{ route('d-post-create') }}">Crear</a>

    <div class="grid grid-cols-2 mb-2 gap-2">
        <x-input class="w-full" type="text" wire:model.live.debounce.250ms="search" placeholder="Buscar por 'id', titulo o descripción"></x-input>
        <div class="grid grid-cols-2 gap-2">
            <x-input class="w-full" type="date" wire:model.change="from" placeholder="Desde"></x-input>
            <x-input class="w-full" type="date" wire:model.change="to" placeholder="Hasta"></x-input>
        </div>
    </div>

    <form>
        <div class="flex gap-2 mb-2">
            <select class="block w-full" wire:model.change="posted">
                <option value="">{{ __("Posted") }}</option>
                <option value="yes">Si</option>
                <option value="not">No</option>
        </select>
            
            <select class="block w-full" wire:model.change="type">
                <option value="">{{__("Type")}}</option>
                <option value="adverd">Adverd</option>
                <option value="course">Course</option>
                <option value="Movie">Movie</option>
            </select>

            <select class="block w-full" wire:model.change="category_id">
                <option value="">{{__("Category")}}</option>
                @foreach ($categories as $i => $c)
                    <option value="{{ $i }}">{{ $c }}</option>
                @endforeach
            </select>
        </div>
    </form>

    <table class="table w-full border">
        <thead class="text-left bg-gray-100">
            <tr class="border-b">
                @foreach ($columns as $key => $c)
                    <th class="w-40 border-b p-2"> 
                        <button wire:click="sort('{{ $key }}')">
                            {{ $c }}
                        @if ($key == $sortColumn)
                            @if ($sortDirection == 'asc')
                                &uarr;
                            @else
                                &darr;
                            @endif
                            
                        </button>
                        @endif
                    </th>
                @endforeach

                <th class="border-b p-2 text-center">
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $p)
                <tr class="border-b">
                    <td class="p-2">
                        {{ $p->id }}
                    </td>
                    <td class="p-2">
                        {{ str($p->title)->substr(0,15)}}
                    </td>
                    <td class="p-2">
                        {{ $p->date}}
                    </td>
                    <td class="p-2">
                        <textarea class="w-48" disabled> {{ $p->description}} </textarea>
                    </td>
                    <td class="p-2">
                        {{ $p->posted}}
                    </td>
                    <td class="p-2">
                        {{ $p->type}}
                    </td>
                    <td class="p-2">
                        {{ $p->category->title}}
                    </td>
                    <td class="p-2 text-center">
                        <x-nav-link href="{{ route('d-post-edit', $p)}}" class="mr-3 btn-edit">Editar</x-nav-link>
                        <x-danger-button 
                        {{-- onclick="confirm('¿Seguro que desea eliminar el registro seleccionado?') || event.stopImmediatePropagation()"   --}}
                        wire:click="selectedPostToDelete({{$p}})">
                            Eliminar
                        </x-danger-button>
                        
                    </td>
                    <td>

                    </td>
                </tr>
            @endforeach
        </tbody>     
    </table>

    <br>

    {{ $posts->links() }}

    <x-confirmation-modal wire:model.live="conmfirmingDeletePost">
        <x-slot name="title">
            {{ __('Borrar Categoría') }}
        </x-slot>

        <x-slot name="content">
            {{ __('¿Estás seguro de querer eliminar esta categoria?') }}
        </x-slot>

        <x-slot name="footer">
            <x-secondary-button wire:click="$toggle('conmfirmingDeletePost')" wire:loading.attr="disabled">
                {{ __('Cancelar') }}
            </x-secondary-button>

            <x-danger-button class="ms-3" wire:click="delete" wire:loading.attr="disabled">
                {{ __('Eliminar') }}
            </x-danger-button>
        </x-slot>
    </x-confirmation-modal>

</x-card>
@script
    <script>
        window.onload = function(){
            Livewire.hook("morph.updated", (el,component)=>{
                console.log(el);
            })
        }
    </script>
@endscript
