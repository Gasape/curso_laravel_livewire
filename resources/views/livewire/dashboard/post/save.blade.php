<div>
    @slot('header')
    {{ __("CRUD posts")}}
@endslot
    <div class="container">
        <x-action-message on="created">
            <div class="box-action-message">
                {{ __('Creado')  }}
            </div>
        </x-action-message>

        <x-action-message on="update">
            <div class="box-action-message">
                {{ __('Actualizado')  }}
            </div>
        </x-action-message>

        <x-form-section submit="submit">

            @slot('title')
                {{ __('Posts') }}
            @endslot

            @slot('description')
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Labore, blanditiis officiis. Ab suscipit facere quam ratione excepturi quo distinctio ullam laboriosam assumenda laudantium culpa dolor, ipsam dolore officiis sint perferendis?
            @endslot
        
            @slot('form')
                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Titulo</x-label>
                    <x-input-error for="title"/>
                    <x-input class="block w-full" type="text" wire:model="title"/>
                    
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Fecha</x-label>
                    <x-input-error for="date"/>
                    <x-input class="block w-full" type="date" wire:model="date"/>  
                </div>

                <div class="col-span-6 sm:col-span-4">
                   <div wire:ignore >
                        <textarea id="editor">{!! $text !!}</textarea>
                   </div>
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Contenido</x-label>
                    <x-input-error for="description"/>
                    <textarea class="block w-full" wire:model="description"></textarea>
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Descripcion</x-label>
                    <x-input-error for="text"/>
                    <textarea class="block w-full" wire:model="text" id='text'></textarea>
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Posted</x-label>
                    <x-input-error for="posted"/>
                   <select class="block w-full" wire:model="posted">
                        <option value="">Seleccione un valor</option>
                        <option value="yes">Si</option>
                        <option value="not">No</option>
                   </select>
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Tipo</x-label>
                    <x-input-error for="type"/>
                   <select class="block w-full" wire:model="type">
                    <option value="">Seleccione un valor</option>
                        <option value="adverd">Adverd</option>
                        <option value="course">Course</option>
                        <option value="movie">Movie</option>
                   </select>
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <x-label for="">Categorias</x-label>
                    <x-input-error for="category_id"/>
                   <select class="block w-full" wire:model="category_id">
                        <option value=""></option>
                        @foreach ($categories as $c)
                            <option value="{{ $c->id }}">{{ $c->title }}</option>
                        @endforeach
                   </select>
                </div>

                <div class="col-span-6 sm:col-span-4">           
                    <x-label for="">Imagen</x-label>
                    <x-input-error for="image"/>
                    <x-input type="file" wire:model="image"/>
                    
                    @if ($post && $post->image)
                    <img class="w-40 my-3" src=" {{ $post->getImageUrl() }}">         
                    @endif
                </div>
            @endslot
            
            @slot('actions')
                <x-button type="submit">Enviar</x-button>
            @endslot
        </x-form-section>

    </div>
</div>

@script
    <script>
    
    
        ClassicEditor.create( document.querySelector( '#editor' )).then(
            editor => {
                editor.model.document.on( 'change:data', () => {
                    // console.log(editor.getData());
                    $wire.$set('text', editor.getData())
                });
                document.querySelector('#text').addEventListener('input', () =>{
                    editor.setData();
                    // console.log(editor)
                });
            }
        ) 
        .catch( error => {
            console.error( error );
        });

        

    </script>
@endscript




