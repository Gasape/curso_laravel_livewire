<div {{ $attributes->merge(['class' =>'min-h-10 flex flex-col sm:justify-center items-center  sm:pt-0 bg-gray-100']) }}>

    <div class="w-full sm:max-w-screen-2xl mt-6  bg-white shadow-md overflow-hidden sm:rounded-lg">
        @if (@isset($title))
            <div class="bg-gray-50 py-3">
                <h2 class="text-3xl text-center"> {{ $title }}</h2>
            </div>    
        @endif
        <div class="px-6 py-4">
            {{ $slot }}
        </div>
    </div>
</div>
